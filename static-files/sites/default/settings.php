<?php

$databases = array();
$update_free_access = FALSE;
$drupal_hash_salt = '';

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

// Disable poormanscron.
$conf['cron_safe_threshold'] = 0;

// Keep logs longer.
$conf['dblog_row_limit'] = 100000;

// 3rd-party account info.
$conf['google_admanager_account'] = 'ca-pub-8747679010629587';

// Disable drupalorg_crosssite navigation.
$conf['drupalorg_navigation'] = FALSE;
$conf['drupalorg_site'] = 'latinamerica2015';
$conf['drupalorg_crosssite_user_picture_style'] = 'thumbnail';
$conf['logintoboggan_pre_auth_role'] = 8;
$conf['user_email_verification'] = FALSE;

// Sync account information.
$conf['bakery_supported_fields'] = array(
  'name' => 'name',
  'mail' => 'mail',
  'status' => 'status',
  'signature' => 'signature',
);

// Memcache configuration.
$conf['memcache_key_prefix'] = 'latinamerica2015.drupal.org';

// Include a common settings file if it exists.
$common_settings = '/var/www/settings.common.php';
if (file_exists($common_settings)) {
  include $common_settings;
}

// Include a local settings file if it exists.
$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
